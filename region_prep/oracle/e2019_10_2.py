#!/usr/bin/python
# -*- coding: UTF-8 -*-

from random import normalvariate
from math import sqrt, exp


R = 100                 # сопротивление резистора
alpha = 15 * 10**(-3)   # коэффициент
T0 = 20                 # комнатная темература
TMax = 60               # максимальная темература
c = 0.5                 # теплоёмкость резистора


print("Зависимость напряжения на резисторе от температуры")
for T in range(20, 60, 2):
    if T == T0:
        U = 0
    else:
        U = normalvariate(sqrt(alpha * R * max(T - T0, 0)), 0.1)
    print("%i град : %.2f В" % (T, U))
print("Зависимость температуры от времени при остывании резистора")
for t in range(0, 180, 10):
    T = T0 + (TMax-T0) * exp(-(alpha/c)*normalvariate(t, .2))
    T = max(normalvariate(T, .5), T0)
    print("%i время (с) : %i град" % (t, T))
